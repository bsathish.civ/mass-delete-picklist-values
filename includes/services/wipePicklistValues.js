const jsforce = require('jsforce');

/**
 * Wipes the picklists
 * @param {Object}  objectName salesforce object name
 * @param {Array} fields fields to wipe out.
 * @param {object} jsforceCon jsforce connection to salesforce 
 */
 module.exports.wipePicklist = async function(objectName="account", fieldArr=["Specialty__c"], instanceUrl, accessToken) {

    try {

        let conn = new jsforce.Connection({
            instanceUrl: instanceUrl,
            accessToken: accessToken,
            version: '48.0'
        })

        let desc = await conn.metadata.read('CustomObject', [objectName]);

        let fieldSet = new Set(
            fieldArr.map((field) => {return field.toLowerCase()})
        );

        let fields = desc.fields.filter(
            field => {
                return fieldSet.has(field.fullName.toLowerCase())
            }
        );

        let wipedFields = [];

        // While emptying the picklist, atleast one value must be retained.

        fields.forEach(
            field => {
                field.valueSet.valueSetDefinition.value = [{default:false, fullName:'default', label:'default'}];
                field.valueSet.valueSettings = [];
                wipedFields.push(
                    {
                        fullName: `${objectName}.${field.fullName}`,
                        type: 'Picklist',
                        label: field.label,
                        description: field.description,
                        valueSet: field.valueSet,
                        inlineHelpText: field.inlineHelpText
                    }
                );

            }
        );
         
        let res = await conn.metadata.update(
            'CustomField',
            wipedFields
        );

        console.log(res);
        
        return true;

    } catch (e) {

        throw e;

    }

 }
