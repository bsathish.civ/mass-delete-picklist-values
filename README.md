# mass-delete-picklist-values

Call this service with below params to wipe the picklists values in your org.

`POST` to `<heroku app name>/wipePicklistValues` with body

`
{
    "instanceUrl": <salesforce org instance url>,
    "accessToken": <accessToken>,
    "objectName" : "account",
    "fields"     : "customField__c, customField2__c"
}
`