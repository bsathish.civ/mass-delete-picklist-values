const express = require("express");
const BodyParser = require("body-parser");
const wiper = require("./includes/services/wipePicklistValues");

// setup express app
const app = express();

// JSON parse body
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: false }));

// setting security headers
app.use((req, res, next) => {
    res.set({
        'X-XSS-Protection': '1; mode=block',
        'Cache-Control': 'no-cache',
        'X-Content-Type-Options': 'nosniff',
        'Content-Security-Policy': 'script-src \'self\''
    });
    return next();
});

// request handlers
app.get('/', (req, res) => 
    {
        res.status(200).send('Microservice utility app is running...');
    }
);

app.post( '/wipePicklistValues', handleWipeRequest );

app.listen(process.env.PORT || 5000, () => {console.log(`listening on ${process.env.PORT || 5000}`);});

/**
 * Prepares the jsforce connection from the access token and instance URL
 * and wipes the picklist values
 * @param {Object} req
 * @param {Object} res
 */
async function handleWipeRequest(req, res) {

    try {

        console.log(req.body);

        let instanceUrl = req.body.instanceUrl;
        let accessToken = req.body.accessToken;
        let objectName  = req.body.objectName;
        let fields      = req.body.fields;

        if (!instanceUrl) { throw new Error("Required parameters are missing: [instanceUrl]") }

        if (!accessToken) { throw new Error("Required parameters are missing: [accessToken]") }

        if (!objectName) { throw new Error("Required parameters are missing: [objectName]") }

        if (!fields) { throw new Error("Required parameters are missing: [fields]") }

        // split the fields
        fields = fields.split(',');


        // instantiate a new wipePicklist object
        let result = await wiper.wipePicklist(objectName, fields, instanceUrl, accessToken);

        if ( result ) {
            return res.send({ message: "Picklist fields have been wiped" });
        } else {
            return res.status(500).send({ message: "Error wiping picklist fields" });
        }

        
    
    } catch (e) {
        console.log(e);
        res.status(500).send(e);

    }

}


